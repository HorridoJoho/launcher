#pragma once

#include "closehandle_deleter.hpp"

#include <Windows.h>

#include <memory>

namespace winapi {

class process;

class thread
{
	friend class process;

public:
	DWORD resume();
	void wait();
	void terminate(UINT i);
	DWORD get_exit_code();
	HANDLE get_handle();

private:
	thread(HANDLE ThreadHandle);

	std::unique_ptr<void, closehandle_deleter> mThreadHandle;
};

} // namespace winapi