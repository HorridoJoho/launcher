#include "thread.hpp"

#include <stdexcept>
#include <string>

using namespace std;

namespace winapi {

DWORD thread::resume()
{
	DWORD res = ResumeThread(mThreadHandle.get());
	if (res == (DWORD)-1)
	{
		throw runtime_error("Failed to resume thread: " + to_string(GetLastError()));
	}
	return res;
}

void thread::wait()
{
	if (WaitForSingleObject(mThreadHandle.get(), INFINITE) != WAIT_OBJECT_0)
	{
		throw runtime_error("Failed to wait for thread!");
	}
}

void thread::terminate(UINT i)
{
	TerminateThread(mThreadHandle.get(), i);
}

DWORD thread::get_exit_code()
{
	DWORD code;
	if (GetExitCodeThread(mThreadHandle.get(), &code) == 0)
	{
		throw runtime_error("Failed to get thread exit code!");
	}
	return code;
}

HANDLE thread::get_handle()
{
	return mThreadHandle.get();
}

thread::thread(HANDLE ThreadHandle)
	: mThreadHandle(ThreadHandle)
{}

} // namespace winapi