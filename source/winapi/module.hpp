#pragma once

#include <Windows.h>

#include <string>

namespace winapi {

class module {
public:
	static module* load(std::wstring name);
	static module* get(std::wstring name);

	~module();

	FARPROC get_proc_address(std::string name);

	HMODULE get_handle();
private:
	module(HMODULE module_handle, bool free_on_destroy);

	HMODULE m_module_handle;
	bool m_free_on_destroy;
};

} // namespace winapi