#include "closehandle_deleter.hpp"

namespace winapi {

void closehandle_deleter::operator()(HANDLE h)
{
	CloseHandle(h);
}

} // namespace winapi