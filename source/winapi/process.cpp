#include "process.hpp"

#include <stdexcept>

using namespace std;

namespace winapi {

process::process(wstring exe, wstring cmd, wstring dir)
{
	auto fullCmdline = exe + L" " + cmd;
	unique_ptr<wchar_t[]> fullCmdlineC(new wchar_t[fullCmdline.length() + 1]);
	wcscpy_s(fullCmdlineC.get(), fullCmdline.length() + 1, fullCmdline.c_str());

	STARTUPINFOW startInfo;
	PROCESS_INFORMATION processInfo;
	ZeroMemory(&processInfo, sizeof(processInfo));
	ZeroMemory(&startInfo, sizeof(startInfo));
	startInfo.cb = sizeof(STARTUPINFO);
	if (!CreateProcessW(exe.c_str(), fullCmdlineC.get(), nullptr, nullptr, FALSE, CREATE_SUSPENDED, nullptr, dir.c_str(), &startInfo, &processInfo))
	{
		CloseHandle(processInfo.hThread);
		CloseHandle(processInfo.hProcess);
		throw runtime_error("Failed to start app! GLE=" + to_string(GetLastError()));
	}

	mProcessHandle.reset(processInfo.hProcess);
	mMainThread.reset(new thread(processInfo.hThread));
}

void process::terminate(UINT i)
{
	TerminateProcess(mProcessHandle.get(), i);
}

LPVOID process::virtual_alloc(LPVOID lpAddress, SIZE_T dwSize, DWORD flAllocationType, DWORD flProtect)
{
	LPVOID pRemoteMemory = VirtualAllocEx(mProcessHandle.get(), lpAddress, dwSize, flAllocationType, flProtect);
	if (pRemoteMemory == nullptr)
	{
		throw runtime_error("Failed to allocate memory: " + to_string(GetLastError()));
	}
	return pRemoteMemory;
}

void process::virtual_free(LPVOID lpAddress, SIZE_T dwSize, DWORD dwFreeType)
{
	VirtualFreeEx(mProcessHandle.get(), lpAddress, dwSize, dwFreeType);
}

void process::write_memory(LPVOID lpBaseAddress, LPCVOID lpBuffer, SIZE_T nSize, SIZE_T* lpNumberOfBytesWritten)
{
	if (WriteProcessMemory(mProcessHandle.get(), lpBaseAddress, lpBuffer, nSize, lpNumberOfBytesWritten) == FALSE)
	{
		throw runtime_error("Failed to write memory: " + to_string(GetLastError()));
	}
}

unique_ptr<thread> process::create_remote_thread(LPSECURITY_ATTRIBUTES lpThreadAttributes, SIZE_T dwStackSize, LPTHREAD_START_ROUTINE lpStartAddress, LPVOID lpParameter, DWORD dwCreationFlags, LPDWORD lpThreadId)
{
	HANDLE hThread = CreateRemoteThread(mProcessHandle.get(), lpThreadAttributes, dwStackSize, lpStartAddress, lpParameter, dwCreationFlags, lpThreadId);
	if (hThread == NULL)
	{
		throw runtime_error("Failed to create remote thread: " + to_string(GetLastError()));
	}
	return unique_ptr<thread>(new thread(hThread));
}

HANDLE process::get_handle()
{
	return mProcessHandle.get();
}

shared_ptr<thread> process::get_main_thread()
{
	return mMainThread;
}

} // namespace winapi