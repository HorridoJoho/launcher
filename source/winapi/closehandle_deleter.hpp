#pragma once

#include <Windows.h>

namespace winapi {

class closehandle_deleter
{
public:
	void operator()(HANDLE h);
};

} // namespace winapi