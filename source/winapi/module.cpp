#include "module.hpp"

#include <stdexcept>

using namespace std;

namespace winapi {

module* module::load(wstring name)
{
	HMODULE module_handle = LoadLibraryW(name.c_str());
	if (module_handle == NULL)
	{
		throw runtime_error("Failed to load library! GLE=" + to_string(GetLastError()));
	}

	return new module(module_handle, true);
}

module* module::get(wstring name)
{
	HMODULE module_handle = GetModuleHandleW(name.c_str());
	if (module_handle == NULL)
	{
		throw runtime_error("Failed to get module handle! GLE=" + to_string(GetLastError()));
	}

	return new module(module_handle, false);
}

module::~module()
{
	if (m_free_on_destroy)
	{
		FreeLibrary(m_module_handle);
	}
}

FARPROC module::get_proc_address(string name)
{
	return GetProcAddress(m_module_handle, name.c_str());
}

HMODULE module::get_handle()
{
	return m_module_handle;
}

module::module(HMODULE module_handle, bool free_on_destroy)
	: m_module_handle(module_handle), m_free_on_destroy(free_on_destroy)
{}

} // namespace winapi