#pragma once

#include "thread.hpp"
#include "closehandle_deleter.hpp"

#include <Windows.h>

#include <string>
#include <memory>

namespace winapi {

class process
{
public:
	process(std::wstring exe, std::wstring cmd, std::wstring dir);

	void terminate(UINT i);

	LPVOID virtual_alloc(LPVOID lpAddress, SIZE_T dwSize, DWORD flAllocationType, DWORD flProtect);
	void virtual_free(LPVOID lpAddress, SIZE_T dwSize, DWORD dwFreeType);

	void write_memory(LPVOID lpBaseAddress, LPCVOID lpBuffer, SIZE_T nSize, SIZE_T* lpNumberOfBytesWritten);

	std::unique_ptr<thread> create_remote_thread(LPSECURITY_ATTRIBUTES lpThreadAttributes, SIZE_T dwStackSize, LPTHREAD_START_ROUTINE lpStartAddress, LPVOID lpParameter, DWORD dwCreationFlags, LPDWORD lpThreadId);

	HANDLE get_handle();
	std::shared_ptr<thread> get_main_thread();
private:
	std::unique_ptr<void, closehandle_deleter> mProcessHandle;
	std::shared_ptr<thread> mMainThread;
};

} // namespace winapi