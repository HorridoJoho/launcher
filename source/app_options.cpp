#include "app_options.h"

#include <Windows.h>

#include <iostream>
#include <fstream>
#include <vector>

using namespace boost::program_options;
using namespace std;

singleton_data<app_options> app_options::s_singleton;

void app_options::ensure()
{
	s_singleton.get();
}

string app_options::get_usage(unsigned int width)
{
	ostringstream oss;
	s_singleton.get()->m_all_opts.print(oss, width);
	return oss.str();
}

shared_ptr<variables_map> app_options::get_all()
{
	return s_singleton.get()->m_vars;
}

bool app_options::has(const APP_OPT& opt)
{
	return s_singleton.get()->m_vars->count(opt.name) > 0;
}

app_options::app_options()
	: m_all_opts(), m_cmdline_opts("Commandline Options"), m_config_opts("Config Options"), m_vars(shared_ptr<variables_map>(new variables_map()))
{
	m_cmdline_opts.add_options()
		// Works fine
		(APP_OPT_HELP.name.c_str(), APP_OPT_HELP.descr.c_str())
		// C2679
		//(APP_OPT_CONFIG.name.c_str(), wvalue<wstring>(), APP_OPT_CONFIG.descr.c_str())
	;

	m_config_opts.add_options()
		(APP_OPT_APP_EXE.name.c_str(), wvalue<wstring>(), APP_OPT_APP_EXE.descr.c_str())
		(APP_OPT_APP_CMDLINE.name.c_str(), wvalue<wstring>(), APP_OPT_APP_CMDLINE.descr.c_str())
		(APP_OPT_APP_DIR.name.c_str(), wvalue<wstring>(), APP_OPT_APP_DIR.descr.c_str())
		(APP_OPT_APP_INJECT.name.c_str(), wvalue<vector<wstring>>(), APP_OPT_APP_INJECT.descr.c_str())
	;

	m_all_opts.add(m_cmdline_opts).add(m_config_opts);

	vector<wstring> cmdlineArgs = split_winmain(GetCommandLineW());
	wcommand_line_parser parser(cmdlineArgs);

	// parse command line
	wparsed_options cmdlineParsed = parser.options(m_all_opts).allow_unregistered().run();
	store(cmdlineParsed, *m_vars);

/*	wstring configFile = DEF_CONFIG_FILE;
	if (m_vars->count(APP_OPT_CONFIG.name))
	{
		configFile = (*m_vars)[APP_OPT_CONFIG.name].as<wstring>();
	}
	wchar_t dir[MAX_PATH];
	GetCurrentDirectoryW(MAX_PATH, dir);
*/

	// load config file
	string configFile = CONFIG_FILE;
	wifstream wif(configFile);
	if (!wif)
	{
		throw ios::failure(("Failed to open config file: " + configFile).c_str());
	}
	wparsed_options configFileParsed = parse_config_file<wchar_t>(wif, m_all_opts, true);
	store(configFileParsed, *m_vars);
	notify(*m_vars);
}
