#pragma once

#include <functional>
#include <memory>

template<class T>
class singleton_data
{
public:
	singleton_data()
		: m_getter(std::bind(&singleton_data<T>::init_and_get, this)), m_object(nullptr)
	{}

	std::shared_ptr<T> get()
	{
		return m_getter();
	}
private:
	std::shared_ptr<T> init_and_get()
	{
		if (!m_object)
		{
			m_object.reset(new T());
		}

		m_getter = std::bind(&singleton_data<T>::only_get, this);
		return m_object;
	}

	std::shared_ptr<T> only_get()
	{
		return m_object;
	}

	std::function<std::shared_ptr<T>()> m_getter;
	std::shared_ptr<T> m_object;
};



