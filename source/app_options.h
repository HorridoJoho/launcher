#pragma once

#include "singleton_data.h"

#include <boost/program_options.hpp>
#include <memory>
#include <string>

struct APP_OPT
{
	const std::string name;
	const std::string descr;
};

const APP_OPT APP_OPT_HELP = { "help", "Show this help." };
//const APP_OPT APP_OPT_CONFIG = { "config", "The path to the config file." };

const APP_OPT APP_OPT_APP_EXE = { "app.exe", "The exe to launch." };
const APP_OPT APP_OPT_APP_CMDLINE = { "app.cmdline","The commandline for the exe." };
const APP_OPT APP_OPT_APP_DIR = { "app.dir", "The working directory for the app." };
const APP_OPT APP_OPT_APP_INJECT = { "app.inject", "A library to inject into the app." };

//const std::wstring DEF_CONFIG_FILE = L"Launcher.cfg";
const std::string CONFIG_FILE = "Launcher.cfg";

class app_options
{
	friend class singleton_data<app_options>;
public:
	static void ensure();
	static std::string get_usage(unsigned int width = 0);
	static std::shared_ptr<boost::program_options::variables_map> get_all();
	static bool has(const APP_OPT& opt);

	template<class T>
	static T get(const APP_OPT& opt)
	{
		return (*s_singleton.get()->m_vars)[opt.name].as<T>();
	}
private:
	app_options();

	boost::program_options::options_description m_all_opts;
	boost::program_options::options_description m_cmdline_opts;
	boost::program_options::options_description m_config_opts;
	std::shared_ptr<boost::program_options::variables_map> m_vars;

	static singleton_data<app_options> s_singleton;
};