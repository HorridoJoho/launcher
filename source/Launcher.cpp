#include "Launcher.h"

#include "app_options.h"
#include "winapi/process.hpp"
#include "winapi/thread.hpp"
#include "winapi/module.hpp"

#include <string>
#include <vector>
#include <exception>
#include <memory>

using namespace std;
using namespace winapi;

static void check_app_option(const APP_OPT& opt);
static void inject_library(process& remoteProcess, wstring library);

int APIENTRY wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPWSTR lpCmdLine, int nCmdShow)
{
	try
	{
        // ensure app options are loaded
		app_options::ensure();

		if (app_options::has(APP_OPT_HELP))
		{
			// show help
			MessageBoxA(NULL, app_options::get_usage(10).c_str(), LAUNCHER_TITLE, MB_ICONINFORMATION);
			return 0;
		}

        // check required app options
		check_app_option(APP_OPT_APP_EXE);
		check_app_option(APP_OPT_APP_CMDLINE);
		check_app_option(APP_OPT_APP_DIR);

		auto exe = app_options::get<wstring>(APP_OPT_APP_EXE);
		auto cmdline = app_options::get<wstring>(APP_OPT_APP_CMDLINE);
		auto dir = app_options::get<wstring>(APP_OPT_APP_DIR);

		process app(exe, cmdline, dir);
        try
        {
            auto injections = app_options::get<vector<wstring>>(APP_OPT_APP_INJECT);
            for (auto injection : injections)
            {
                inject_library(app, injection);
            }

			app.get_main_thread()->resume();
        }
        catch (exception& e)
        {
			app.terminate(1);
            MessageBoxA(NULL, e.what(), LAUNCHER_TITLE, MB_ICONERROR);
            return 2;
        }
	}
	catch (exception& e)
	{
		MessageBoxA(NULL, e.what(), LAUNCHER_TITLE, MB_ICONERROR);
		return 1;
	}

	return 0;
}

void check_app_option(const APP_OPT& opt)
{
	if (!app_options::has(opt))
	{
		throw runtime_error("Missing option " + opt.name + "!");
	}
}

void inject_library(process& remoteProcess, wstring library)
{
	const SIZE_T cbLibrary = library.length() * sizeof(wchar_t) + sizeof(wchar_t);
	if (cbLibrary <= sizeof(wchar_t))
	{
		throw runtime_error("Short library name!");
	}

	LPVOID pRemoteMemory = remoteProcess.virtual_alloc(nullptr, cbLibrary, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);
	try
	{
		remoteProcess.write_memory(pRemoteMemory, library.c_str(), cbLibrary, nullptr);

		unique_ptr<module> k32_module(module::get(L"kernel32.dll"));
		LPTHREAD_START_ROUTINE fnRemoteThread = (LPTHREAD_START_ROUTINE)k32_module->get_proc_address("LoadLibraryW");
		if (fnRemoteThread == nullptr)
		{
			throw runtime_error("Failed to get function address!");
		}

		unique_ptr<thread> remoteThread = remoteProcess.create_remote_thread(nullptr, 0, fnRemoteThread, pRemoteMemory, 0, nullptr);
		try
		{
			remoteThread->wait();
			DWORD dwRemoteThreadExitCode = remoteThread->get_exit_code();
			if (dwRemoteThreadExitCode == 0)
			{
				throw runtime_error("Failed to load library into remote process!");
			}
		}
		catch (exception& e)
		{
			remoteThread->terminate(1);
			throw e;
		}
	}
	catch (exception& e)
	{
		remoteProcess.virtual_free(pRemoteMemory, cbLibrary, MEM_RELEASE);
		throw e;
	}

	remoteProcess.virtual_free(pRemoteMemory, cbLibrary, MEM_RELEASE);
}
